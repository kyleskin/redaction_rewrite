#Adobe Regex Builder
##INSTALLATION
curl -sSL https://get.rvm.io | sudo bash -s stable
gem install rubygems-bundler
gem update --system
sudo gem install bundler

##RUNNING
Adobe -> Preferences -> Documents -> Restore Last View Settings When Opening Documents

The main script is called 'run'
As an example:
`./run -r config/resources.yaml.default`
`resources.yaml.default` specifies paths to files and folders that
hold the patterns and words files to match.


##Tips and Tricks
###Performance
These numbers were tested on a single document with a regex with 318 patterns
and 4097 chars.
> Words: ~14 pages / second
> Patterns: ~ 4 pages / second

Thus, with the same phrases, it appears that patterns are around 3x slower than exact match words. If possible, the user should prefer exact matches ('words') file when possible.

#Adobe Javascript Find-And-Replace Plugin
##Installation & Use
Requires: Adobe Acrobat Pro
1. On OSX, right click on Adobe Acrobat Pro and go to 'show package contents'
2. Copy the .js file into Contents > Resources > Javascripts folder and restart adobe pro
3. It should show up as a plugin
4. Open up: MarkRedact3-14.pdf, this is a special pdf input the redacted file into, and it generates a text file
5. then you edit the text file to replace the redacted values, with new values, i.e. 123 > ABC, and run it to replace them