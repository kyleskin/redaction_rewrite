
ENCODING_OPTIONS = {
  :invalid           => :replace,  # Replace invalid byte sequences
  :undef             => :replace,  # Replace anything not defined in ASCII
  :replace           => ' ',        # Use a blank for those replacements
  :universal_newline => true       # Always break lines with \n
}

ASCII_ENCODING = Encoding.find('ASCII')
BINARY_ENCODING = Encoding.find('BINARY')

class String
  def remove_non_ascii
    # Unfortunately its a No-OP if the encoding is already read as ascii
    # so we have to force binary then re-encode ascii
    self.force_encoding(BINARY_ENCODING).encode(ASCII_ENCODING, ENCODING_OPTIONS)
  end
end