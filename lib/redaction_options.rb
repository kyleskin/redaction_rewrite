require 'rubygems'
require 'bundler/setup'
require 'yaml'
require 'pathname'
require_relative 'encodings'
require_relative 'os'
require 'pry'

class RedactionOptions < Struct.new(:root_folder,
                                    :application_config_filepath,
                                    :application_config,
                                    :install_path,
                                    :cached_file_path,
                                    :template_filepath,
                                    :template,
                                    :resources_config_filepath,
                                    :resources_config,
                                    :patterns_filepath,
                                    :patterns,
                                    :words_filepath,
                                    :words,
                                    :filepath_fields,
                                    :verbose)
  

  VALID_FILE_EXTENSIONS = ['txt']

  def initialize(*args)
    super
    
    self.filepath_fields = [
      :application_config_filepath,
      :template_filepath,
      :resources_config_filepath,
      :patterns_filepath,
      :words_filepath,
    ]

    self.application_config_filepath ||= File.join(self.root_folder, 'config', 'application.yaml')
    self.template_filepath ||= File.join(self.root_folder, 'templates', 'SearchRedactPatterns.template.xml')
  end

  def validate!

    #THESE VALIDATIONS ARE ORDER DEPENDEDNT! DOnt switch ordering!
    validate_command_line_args
    validate_file_existence
    validate_adobe_paths #install_path and cached_file_path
    validate_template
    validate_resources
  end

  private
  
  def validate_command_line_args
    if !resources_config_filepath && !words_filepath && !patterns_filepath
      puts "You didn't specify any words or pattern files.\n" +
      "Please specify paths to words_filepath using command line options:\n" + 
      " ./run (-w WORD_FILE) and patterns filepath (-p PATTERNS_FILE) or\n" +
      "to a resource file with the proper paths (-r RESOURCE_FILE)"
      exit
    end
  end

  def validate_adobe_paths
    read_application_config

    puts "Using config for: #{OS::os}" if self.verbose

    os_config = self.application_config['adobe'][OS::os]
    self.install_path = resolve_file_or_glob_path(os_config['install_path'])
    #optional param, only used on windows
    self.cached_file_path = resolve_file_or_glob_path(os_config['cached_file_path']) if os_config['cached_file_path']

    validate_file_exists(self.install_path)
    validate_file_exists(self.cached_file_path) if self.cached_file_path
  end

  def resolve_file_or_glob_path(path)
    full_path = File.expand_path(path)
    
    #Check if using glob syntax
    if path.index('*')
      puts "Resolving glob: #{full_path}..." if self.verbose
      full_path = Dir.glob(full_path).sort.last
      puts "Resolved to: #{full_path}" if self.verbose
    end

    full_path
  end

  #validates template xml file that values are substituted into
  def validate_template
    read_template
  end

  # Sets patterns and words filepaths
  # Validates files exist, and at least one is a valid file
  # Reads the files as strings
  def validate_resources
    if resources_config_filepath
      read_resources_config
      
      self.patterns_filepath ||= resources_config['patterns_filepath']
      self.words_filepath ||= resources_config['words_filepath']
    end

    if !self.patterns_filepath && !self.words_filepath
      raise "No patterns files specified in command line args or resources config. Please specify resource files and retry."
      exit
    end
    
    if self.patterns_filepath
      puts "Reading patterns files..." if self.verbose
      self.patterns = read_resources(self.patterns_filepath)
      puts "Got #{self.patterns.length} patterns." if self.verbose
    end
    
    if self.words_filepath
      puts "Reading words files..." if self.verbose
      self.words = read_resources(self.words_filepath)
      puts "Got #{self.words.length} exact match patterns."
    end

    num_patterns = self.patterns ? self.patterns.length : 0
    num_words = self.words ? self.words.length : 0

    if (num_patterns + num_words) == 0 
      raise "Number of patterns read was zero. Please specify some patterns and save the resource files, then retry compilation."
      exit
    end
  end

  def read_resources(filepaths)
    #convert to an array if it is a single string, otherwise keep it as array
    filepaths = Array(filepaths)

    #if its an Array of filepath / folders
    all_paths = filepaths.collect do |f|
      f = make_full_path(f)

      if File.directory?(f)
        puts "Expanding Directory: #{f}" if self.verbose
        valid_files_from_folder(f)
      elsif VALID_FILE_EXTENSIONS.include?(File.extname(f).tr('.',''))
        f
      end
    end.flatten.compact

    puts "Resource Files: #{all_paths.join("\n")}" if self.verbose
    final_resource = all_paths.inject([]) do |memo, path| 
      memo.concat parse_resource_file(path)
    end
  
    final_resource
  end

  def make_full_path(filepath)
    if (Pathname.new filepath).relative?
      puts "Expanding Relative Path: #{filepath}" if self.verbose
      filepath = File.join(root_folder, filepath)
      puts "Expanded to: #{filepath}" if self.verbose
    end
    
    filepath = File.expand_path(filepath)
  end

  def valid_files_from_folder(folder_path)
    Dir.glob(File.join(folder_path, "*.{#{VALID_FILE_EXTENSIONS.join(',')}}"))
  end

  def parse_resource_file(filepath)
    File.read(filepath).remove_non_ascii.gsub(/\r\n?/, "\n").split("\n").collect {|x| x.strip}.select {|x| x.length > 0}.uniq
  end

  def read_template
    self.template = File.read(template_filepath)
  end

  def read_resources_config
    self.resources_config = YAML.load_file(resources_config_filepath)
  end

  def read_application_config
    self.application_config = YAML.load_file(application_config_filepath)
  end

  def validate_file_existence
    filepath_fields.each do |field|
      filepath_value = self.send(field)
      if filepath_value
        validate_file_exists(filepath_value)
      end
    end
  end

  def validate_file_exists(path)
    if !File.exist?(path)
      puts "Error: file path doesn't exist: #{path}"
      raise "Error: file path doesn't exist: #{path}"
    end
    path
  end
end
