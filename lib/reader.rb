require 'treat'
require 'pdf-reader'
  
class EntityFinder
  include Treat::Core::DSL

  def find_in_pdf(filename)
    reader = PDF::Reader.new(filename)

    matched_words = Hash.new(0)

    txt = ''
    reader.pages.each_with_index do |page,i|
      puts "Reading page: #{i}"
      txt << page.text + "\n"
    end
      p = paragraph(page.text)
      p.apply(:chunk, :segment, :tokenize, :name_tag)
      p.words.select {|x| x[:name_tag]}.each do |matched_word|
        puts "Matched: #{matched_word}"
        matched_words[matched_word] += 1
      end
    end

    matched_words
  end
end

reader = PDF::Reader.new("unredacted.pdf")
txt = reader.pages.each.text
p = paragraph(txt)
p.apply(:chunk, :segment, :tokenize, :name_tag)

p.print_tree

class Reader
  include Treat::Core::DSL
end
