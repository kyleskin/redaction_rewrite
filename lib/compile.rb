#!/usr/bin/env ruby

require 'rubygems'
require 'bundler/setup'
require 'nokogiri'
require 'date'
require 'yaml'
require 'fileutils'
# require 'pry'

class RegexCompiler
  attr_accessor :options, :install_path, 
  :cached_file_path, :words, :patterns, 
  :template, :verbose

  # It seems adobe has a limit to this many chars
  # for a regex, found via testing
  MAX_REGEX_CHARS_LENGTH = 4095

  def initialize(options)
    @options = options
    @install_path = options.install_path
    @cached_file_path = options.cached_file_path #optional, used for windows
    @words = options.words
    @patterns = options.patterns
    @template = options.template
    @verbose = options.verbose
  end

  def run(outfile=nil)
    puts "Starting Compilation..."
    outfile ||= "SearchRedactPatterns.xml"
    
    xml = Nokogiri::XML(@template)
    
    full_regex_strs.each.with_index do |frs, i|

      xml_node = build_xml_node(frs, 
        set_name: "Entry#{6+i}",
        display_name: "Redaction Template #{i+1} of #{full_regex_strs.length}")

      append_xml_node(xml, xml_node)
    end

    write(xml.to_xml, outfile)
    rotate_file(outfile)
  end

  def full_regex_strs
    @full_regex_strs ||= begin
      regex_strings = build_regex_list(words, :words).concat Array(build_regex_list(patterns, :patterns))
      build_max_length_regexes(regex_strings)
    end
  end

  # Make sure the regexes dont go beyond mAX_REGEX_CHARS_LENGTH
  # and if they do, split it up to build multiple regexes instead
  # This incrementally builds regexes 
  def build_max_length_regexes(regex_strings, max_len=MAX_REGEX_CHARS_LENGTH)
    puts "Building Max lengths..." if self.verbose
    final_regexes = []

    return final_regexes if regex_strings.empty?
    current_regex = regex_strings.shift

    check_regex_size(current_regex, max_len)

    #Handle case where regex_strings original length is 1
    if regex_strings.empty?
      final_regexes << current_regex
    end

    regex_strings.each.with_index do |phrase, i|
      check_regex_size(phrase, max_len)

      if current_regex.length + phrase.length < max_len
        current_regex << ('|' + phrase)
      else
        final_regexes << current_regex
        current_regex = phrase
      end

      #if its the last phrase, just put it in the array
      if (regex_strings.length - 1 == i)
        final_regexes << current_regex
      end
    end

    if self.verbose
      puts "Built #{final_regexes.length} regexes:"
      final_regexes.collect.with_index do |x,i| 
        puts "Regex #{i+1}: #{x.length} chars"
      end
    end

    final_regexes
  end

  def check_regex_size(reg_phrase, max_len)
    raise "Regex must be less than #{max_len} chars" if reg_phrase.length > max_len
  end

  #regex_type is symbols: :patterns or :words
  def build_regex_list(vals, regex_type)
    method = "#{regex_type}_to_regex_str".to_sym
    if vals.nil? or vals.empty?
      puts "No #{regex_type} found. Proceeding with Empty #{regex_type} list." if verbose
      nil #just to be explicit
    else
      puts "** Building Regex List: #{regex_type} ***"
      puts "Found #{vals.length} #{regex_type}." if verbose
      regexes_list = vals.collect {|s| self.class.send(method, s)}
    end
  end

  def self.words_to_regex_str(s)
    "\\b#{Regexp.escape(s)}\\b"
  end

  def self.patterns_to_regex_str(p)
    "(#{p})"
  end

  def append_xml_node(xml, xml_node)
    xml.css('asf').first.add_child(xml_node)
  end

  def build_xml_node(regex_string, 
    set_name:'Entry6', 
    display_name: 'Redaction',
    description: nil)
    
    puts "Building XML Node with name: #{display_name}"
    description ||= "This is a custom redaction template created at #{DateTime.now}"

    builder = Nokogiri::XML::Builder.new do |xml|
      xml.set('name' => set_name) {
        xml.str('name'=>'displayName') {
          xml.val display_name
        }
        xml.str('name' => 'regEx', 'translate' => 'no') {
          xml.val regex_string
        }
        xml.str('name' => 'examples') {
          xml.val description
        }
      }
    end.doc.root
  end

  def write(data, outfile)
    File.open(outfile, 'wb') do |f|
      f.puts(data)
    end
  end

  def rotate_file(new_file)
    if File.exist?(install_path)
      #windows doesnt allow colons in filenames so replace with underscore
      bak_filepath = install_path + '.bak.' + DateTime.now.to_s.gsub(':','_')
      File.rename(install_path, bak_filepath)
      puts "Rotated File. Bakfile created: #{bak_filepath}"

      FileUtils.cp(new_file, File.dirname(install_path))
      puts "Succesfully installed to #{install_path}"
    else
      puts "Exiting because adobe install path doesn't exist: #{install_path}"
      puts "Please set correct path in config/application.yaml to SearchRedactPatterns.xml"
      exit
    end

    # Adobe on windows also copies searchredactpatterns.xml to user appdata/roaming folder
    # so we need to delete it so it gets the new version
    if cached_file_path && File.exist?(cached_file_path)
      puts "Deleting cached file: #{cached_file_path}..." if verbose
      File.delete(cached_file_path)
    end
  end
end

# RegexCompiler.new('redactions.txt','patterns.txt').run