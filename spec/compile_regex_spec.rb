require 'compile'
require 'ostruct'
require 'pry'
describe RegexCompiler do
  before :all do

    options = RedactionOptions.new#OpenStruct.new(words: , patterns: , template: )
    compiler = RegexCompiler.new

    #PDF assumes case insensitivity
    @reg = Regexp.new(compiler.full_regex_str, true)
  end

  it "should match demographics" do
    expect(@reg.match 'Asian').to_not be_nil
    expect(@reg.match 'Female').to_not be_nil
  end

  it "should not match single chars" do
    expect(@reg.match 'a').to be_nil
  end

  it "should not match empty string or single space" do
    expect(@reg.match '').to be_nil
    expect(@reg.match ' ').to be_nil
  end

  it "should match user ids" do
    expect(@reg.match 'A12345678').to_not be_nil
  end

  it "should match 00000000(AUSTRIA/UNK/00 years/WHITE/00)" do
    expect(@reg.match '00000000(AUSTRIA/UNK/00 years/WHITE/00)').to_not be_nil
  end

  it "should match 00000000(AUSTRIA/MALE/00 years/WHITE/UNK)" do
    expect(@reg.match '00000000(AUSTRIA/MALE/00 years/WHITE/UNK)').to_not be_nil
  end

  it "should match 10811002(BRAZIL/FEMALE/63 years/OTHER (MIXED)/79)" do
    expect(@reg.match '10811002(BRAZIL/FEMALE/63 years/OTHER (MIXED)/79)').to_not be_nil
  end
end
'10291002(UNITED STATES/MALE/65 years/WHITE/77)'