var annots = getAnnots();

var annot = annots[1];

var rct = annot.rect
var left = rct[0];
var right = rct[2];
var top = rct[3];
var bot = rct[1];
qd = [ [left, top, right, top, left, bot, right, bot] ];
console.println(qd);

this.addAnnot({
  type: "Redact",
  page: this.pageNum,
  quads: qd,
  overlayText: "GONE!",
  alignment: 0,
  repeat: false
});

this.applyRedactions ({
	aMarks: myMarks,
	bKeepMarks: false,
	bShowConfirmation: false,
	cProgText: "It’s going away"
});
