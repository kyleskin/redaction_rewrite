reader = PDF::Reader.new("unredacted.pdf")
page_text = reader.pages[0..10].collect {|x| x.text}.join("\n").remove_non_ascii
File.open("./spec/fixtures/unredacted_10_page.txt",'wb') do |f|
  f.write page_text
end