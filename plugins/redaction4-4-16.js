/*
Place in Javascripts folder
Copyright Richard Grear
License internal use Stephen Doogan
*/

var findReplace1 = app.trustedFunction(function() 
{
	app.beginPriv();


var sQuads= new Array();
var sQuads2 = new Array();
var strReport = "Report: ";
var blnConcat = false;
var intMaxCharForAnnot = new Array();
var intInitPageRotate;
var intTextDirection;//right,down,left,up

//validate all fields
//none empty
//textLocationTB must end in .txt
//pdfLocationTB must end in .pdf
var strValidationMsg = "";
var textLocTBVal = this.getField("textLocationTB").value;
var pdfLocTBVal = this.getField("pdfLocationTB").value;
if(textLocTBVal === ""){    
     strValidationMsg = "\nNo text file specified.";    
}

if(pdfLocTBVal ===""){
      strValidationMsg = strValidationMsg + "\nNo pdf file specified.";
}


if(textLocTBVal.slice(-4).toUpperCase() != ".TXT" && textLocTBVal !=""){    
     strValidationMsg = strValidationMsg + "\nText file should have a .txt extension";    
}

if(pdfLocTBVal.slice(-4).toUpperCase() != ".PDF" && pdfLocTBVal !=""){
      strValidationMsg = strValidationMsg + "\nPDF file should have a .pdf extension.";
}

if(strValidationMsg !=""){
app.alert("Please correct the following and try again:\n" + strValidationMsg,1,0,"Process canceled");
return;
}

/////////////////////////////////////////////////IMPORT TEXT DATA

//Read file data into stream
var stmFileData = util.readFileIntoStream(textLocTBVal);

// Convert data into a String
var strTextData = util.stringFromStream(stmFileData);

//Convert string to data table
var strLines = strTextData.split(/\r\n|\r|\n/g);
//app.alert("#lines:" + strLines.length);

for(i=0;+i<+strLines.length;i++){
var strLineArray = strLines[i].split("\t");
strLines[i] = strLineArray;
}

var strTableValidation = "";
//validate the fields
if(strLines[0][0].trim().toUpperCase() != "TEXT TO BE REPLACED" || strLines[0][1].trim().toUpperCase() != "REPLACE WITH THIS TEXT" || strLines[0][2].trim().toUpperCase() != "PAGE NUMBER" || strLines[0][3].trim().toUpperCase() !="PAGE INSTANCE"){
strTableValidation = "\nIncorrect field names or field order. The first row should begin with the following fields:\nText to be Replaced, Replace with this Text, Page Number, Page Instance";
}

//validate the number of rows
if(+strLines.length < +2){
strTableValidation = strTableValidation + "\nNo data present in text file";
}

if(strTableValidation !=""){
app.alert("Please correct the following in the text file:\n" + strTableValidation,1,0,"Process canceled");
return;
}

/////////////////////////////////////////////////Process the PDF
var th = app.thermometer;
th.duration = strLines.length - 1;

th.begin();

var pdfDoc = app.openDoc({cPath:pdfLocTBVal,bHidden:true});

//clear existing annotations
pdfDoc.syncAnnotScan();
var annotsFnd = pdfDoc.getAnnots();
if (annotsFnd !=null && annotsFnd.length > 0){
for (var m in annotsFnd){
 if(annotsFnd[m].type == "Redact"){
   annotsFnd[m].destroy();
}
}
}

//for each item in the table
for(i=1;+i<+strLines.length;i++){//for
th.value= i;

var blnFound = false;
intMaxCharForAnnot =[];

if(+strLines[i].length > +2){//if5
//get redaction info
var strToFind = strLines[i][0];
var strReplaceWth = String(strLines[i][1]);


th.text = "Processing item " + i + " of " + (strLines.length -1);

//remove special characters from strToFind
strToFind = strToFind.replace(/[^0-9a-zA-Z]/g,"");
//app.alert(strToFind);

//validate pg number. if not a number or out of range then exit
if(isNaN(strLines[i][2]) === true){//if9
//exit
pdfDoc.closeDoc();
th.end();
app.alert("NaN error. Invalid pg number specified on row " + (i + 1),1,0,"Process canceled");
return;
}//end if9

var strOnPgNum = strLines[i][2] -1;//0 based

//validate instance number. if not a number or out of range then exit
if(isNaN(strLines[i][3]) === true){//if9b
//exit
pdfDoc.closeDoc();
th.end();
app.alert("NaN error. Invalid pg instance number specified on row " + (i + 1),1,0,"Process canceled");
return;
}//end if9b

var intPgInstance = strLines[i][3];
var intCurrInstance = 0;

if(+strOnPgNum < +0 || +strOnPgNum > +pdfDoc.numPages - 1){//if10
//exit
pdfDoc.closeDoc();
th.end();
app.alert("Out of range error. Invalid pg number specified on row " + (i + 1),1,0,"Process canceled");
return;
}//end if10





//find and mark one area for redaction based on info
var len = pdfDoc.getPageNumWords(strOnPgNum);

for(n=0;+n<+len;n++){//for 4. for each word in the page

var wd = pdfDoc.getPageNthWord(strOnPgNum,n,true).replace(/[^0-9a-zA-Z]/g,"");

var a = 0;//expand wd if part of strToFind, until equal in length.
blnConcat=false;
while(strToFind.toUpperCase().indexOf(wd.toUpperCase()) !=-1 && +wd.length<+strToFind.length && n+(a+1) < +len){
//ie while not equal in length, and more words are available on page
a = a+1;
wd= wd.concat(pdfDoc.getPageNthWord(strOnPgNum,n+ a,true).replace(/[^0-9a-zA-Z]/g,""));

//intMaxCharForAnnot.push(pdfDoc.getPageNthWord(strOnPgNum,n+a,false).length);

blnConcat=true;
}//end while





if(wd.toUpperCase()===strToFind.toUpperCase()){//if1
intMaxCharForAnnot = [];//fix 4-4
intMaxCharForAnnot.push(pdfDoc.getPageNthWord(strOnPgNum,n,false).length);
intCurrInstance = +intCurrInstance + 1;
if(intCurrInstance == intPgInstance){//if80


if(blnConcat==true){
//mark wd area for redaction.get quads for n, and get quads for n +a, and assemble quad from that.
var qIndex=0;
sQuads=[];
intMaxCharForAnnot=[];

for(f=n;+f<=(n+a);f++){
sQuads[qIndex] = pdfDoc.getPageNthWordQuads(strOnPgNum,f);
intMaxCharForAnnot.push(pdfDoc.getPageNthWord(strOnPgNum,f,false).length);
sQuads[qIndex] = sQuads[qIndex].toString().split(",");
qIndex=qIndex+1;
}

addComplAnnot(pdfDoc,sQuads,strOnPgNum,strReplaceWth,intMaxCharForAnnot);

blnFound=true;

}else{
//perform simple mark. limit replacement string length to intMaxCharForAnnot
var strSub ="";


sQuads=[];
sQuads = pdfDoc.getPageNthWordQuads(strOnPgNum,n);
blnFound=true;

//for the quad about to be applied, there might be two quads applied instead. check for double listing. assign appropriate max characters to new annotation from width to width ratio, and place that annotation as well.
var sQuadsAdd = [];
if(sQuads.length > 1){//if4
//app.alert(sQuads[0]);
//app.alert(sQuads[1]);
sQuadsAdd[0] = [sQuads[1][0],sQuads[1][1],sQuads[1][2],sQuads[1][3],sQuads[1][4],sQuads[1][5],sQuads[1][6],sQuads[1][7]];
var sQuadsAddTxtDir = getTextDirection(sQuadsAdd);
//normalize sQuadsAdd
//if(+sQuadsAdd[0][0]>sQuadsAdd[0][4]){sQuadsAdd[0][0]=sQuadsAdd[0][4];}
//if(+sQuadsAdd[0][1]<sQuadsAdd[0][3]){sQuadsAdd[0][1]=sQuadsAdd[0][3];}
//if(+sQuadsAdd[0][2]<sQuadsAdd[0][6]){sQuadsAdd[0][2]=sQuadsAdd[0][6];}
//if(+sQuadsAdd[0][5]>sQuadsAdd[0][7]){sQuadsAdd[0][5]=sQuadsAdd[0][7];}
//if(+sQuadsAdd[0][0]<sQuadsAdd[0][4]){sQuadsAdd[0][4]=sQuadsAdd[0][0];}
//if(+sQuadsAdd[0][1]>sQuadsAdd[0][3]){sQuadsAdd[0][3]=sQuadsAdd[0][1];}
//if(+sQuadsAdd[0][2]>sQuadsAdd[0][6]){sQuadsAdd[0][6]=sQuadsAdd[0][2];}
//if(+sQuadsAdd[0][5]<sQuadsAdd[0][7]){sQuadsAdd[0][7]=sQuadsAdd[0][5];}


var totalAnnotWidth;
var addAnnotWidth;
if(sQuadsAddTxtDir == "right"){
	totalAnnotWidth = +(sQuadsAdd[0][2] - sQuadsAdd[0][0]) + (+sQuads[0][2] - sQuads[0][0]);
	addAnnotWidth = sQuadsAdd[0][2] - sQuadsAdd[0][0];
	
}else if(sQuadsAddTxtDir == "down"){
	totalAnnotWidth = +(sQuadsAdd[0][1] - sQuadsAdd[0][3]) + (+sQuads[0][1] - sQuads[0][3]);
	addAnnotWidth = sQuadsAdd[0][1] - sQuadsAdd[0][3];
	
}else if(sQuadsAddTxtDir == "left"){
	totalAnnotWidth = +(sQuadsAdd[0][0] - sQuadsAdd[0][2]) + (+sQuads[0][0] - sQuads[0][2]);
	addAnnotWidth = sQuadsAdd[0][0] - sQuadsAdd[0][2];
	
}else if(sQuadsAddTxtDir == "up"){
	totalAnnotWidth = +(sQuadsAdd[0][3] - sQuadsAdd[0][1]) + (+sQuads[0][3] - sQuads[0][1]);
	addAnnotWidth = sQuadsAdd[0][3] - sQuadsAdd[0][1];
	
}


var intMaxLenAdd= Math.round((addAnnotWidth * intMaxCharForAnnot[0]) / totalAnnotWidth);
var intMaxLenOrig = intMaxCharForAnnot[0] - intMaxLenAdd;
intMaxLenAdd = intMaxLenAdd - 1;//-1 added for text size
intMaxLenOrig = intMaxLenOrig - 1;//-1 added for text size

var strReplaceWth3 = "";
var blnDash = false;
	for(jd2 = 0;jd2<intMaxLenOrig;jd2++){
		if(strReplaceWth[jd2]!=undefined){
			if(strReplaceWth[jd2 + 1]!=undefined){
				if(strReplaceWth[jd2]!=" " && strReplaceWth[jd2+1]!=" " && jd2==intMaxLenOrig - 1){
					blnDash = true;
					strSub = strSub.concat("-"); 
				}else{
					strSub = strSub.concat(strReplaceWth[jd2]);

				}

			}else{
			strSub = strSub.concat(strReplaceWth[jd2]);

			}
			 
		}
	}
	
//placed text should be in correct orientation with redaction
intInitPgRotate = pdfDoc.getPageRotation(strOnPgNum);//3-31
rotatePgToAnnot(pdfDoc,sQuads,strOnPgNum);//3-31



pdfDoc.addAnnot({
           page:strOnPgNum,
           type:"Redact",
           quads:sQuads,
           overlayText:strSub,
           alignment:1,
           repeat:false,
           fillColor:color.white,
           textColor:color.black,
           textSize:0,
           textFont:"TimesNewRoman"
      });

if(intInitPgRotate != pdfDoc.getPageRotation(strOnPgNum)){
pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,intInitPgRotate);///3-31
}
	strSub = "";

if(blnDash==false){
	for(jd2 = intMaxLenOrig;jd2<intMaxLenOrig + intMaxLenAdd;jd2++){
		if(strReplaceWth[jd2]!=undefined){
			strSub = strSub.concat(strReplaceWth[jd2]);
			 
		}
	}
}else{
	for(jd2 = intMaxLenOrig - 1;jd2<(intMaxLenOrig - 1) + intMaxLenAdd;jd2++){
		if(strReplaceWth[jd2]!=undefined){
			strSub = strSub.concat(strReplaceWth[jd2]);
			 
		}
	}
}

//placed text should be in correct orientation with redaction
intInitPgRotate = pdfDoc.getPageRotation(strOnPgNum);//3-31
rotatePgToAnnot(pdfDoc,sQuadsAdd,strOnPgNum);//3-31


pdfDoc.addAnnot({
           page:strOnPgNum,
           type:"Redact",
           quads:sQuadsAdd,
           overlayText:strSub,
           alignment:1,
           repeat:false,
           fillColor:color.white,
           textColor:color.black,
           textSize:0,
           textFont:"TimesNewRoman"
      });
if(intInitPgRotate != pdfDoc.getPageRotation(strOnPgNum)){
pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,intInitPgRotate);///3-31
}

}else{//add the annotation normally else4
//app.alert("length = to 8");
//app.alert(sQuads);
	for(jd = 0;jd<intMaxCharForAnnot[0];jd++){
		if(strReplaceWth[jd]!=undefined){
			strSub = strSub.concat(strReplaceWth[jd]);
		}
	}
//placed text should be in correct orientation with redaction
intInitPgRotate = pdfDoc.getPageRotation(strOnPgNum);//3-31
rotatePgToAnnot(pdfDoc,sQuads,strOnPgNum);//3-31

	pdfDoc.addAnnot({
           page:strOnPgNum,
           type:"Redact",
           quads:sQuads,
           overlayText:strSub,
           alignment:1,
           repeat:false,
           fillColor:color.white,
           textColor:color.black,
           textSize:0,
           textFont:"TimesNewRoman"
      });
if(intInitPgRotate != pdfDoc.getPageRotation(strOnPgNum)){
pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,intInitPgRotate);///3-31
}
}//end if4


}



}//end if80
}//end if1

}//end for4 

//apply redaction
//pdfDoc.applyRedactions ({
//	bKeepMarks: false,
//	bShowConfirmation: false,
//	cProgText: "Applying redaction"
//});


if(blnFound==false){
strReport = strReport + "\n\nItem not found: " + strLines[i][0] + "\nOn pg: " + (strOnPgNum + 1) +"\nInstance: " +intPgInstance;
}


}else if (+strLines[i].length > +0){//else if5
strReport = strReport + "\n\nItem not found due to incomplete row in table. Item not found: " + strLines[i][0];
}


}//Next

////testing
//pdfDoc.syncAnnotScan();
//annotsFnd = pdfDoc.getAnnots();
//app.alert(annotsFnd.length);
////

th.text="Applying annotations";
//apply redactions
pdfDoc.applyRedactions ({
	bKeepMarks: false,
	bShowConfirmation: false,
	cProgText: "Applying redactions"
});

//save the pdf as new file
pdfDoc.saveAs(pdfLocTBVal.substring(0,pdfLocTBVal.length - 4) + "_New.pdf");



//do not open the pdf. close.
pdfDoc.closeDoc();

if(strReport == "Report: "){
strReport = "Process completed.";
}
th.end();
this.getField("reportTB").value=strReport;

app.alert("New PDF has been saved.",3,0,"Success");

//addin can be called at end to identify the document, format and apply the redaction marks instead, save and close the document, and update the status report
//app.execMenuItem("PluginMenuName");

app.endPriv();
});


function rotatePgToAnnot(pdfDoc,sQuads,strOnPgNum){
if(+sQuads[0][1] == sQuads[0][3] && +sQuads[0][3] > sQuads[0][5]){
//set page to 0
if(pdfDoc.getPageRotation(strOnPgNum) != 0){pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,0);}

}else if(+sQuads[0][0] == sQuads[0][2] && +sQuads[0][2] > sQuads[0][4]){
if(pdfDoc.getPageRotation(strOnPgNum) != 270){pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,270);}

}else if(+sQuads[0][1] == sQuads[0][3] && +sQuads[0][3] < sQuads[0][5]){
if(pdfDoc.getPageRotation(strOnPgNum) != 180){pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,180);}

}else{
if(pdfDoc.getPageRotation(strOnPgNum) != 90){pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,90);}

}

}


function getTextDirection(sQuads){
if(sQuads[0][1] == sQuads[0][3] && +sQuads[0][3] > +sQuads[0][5]){
return "right";

}else if(sQuads[0][0] == sQuads[0][2] && +sQuads[0][2] > +sQuads[0][4]){
return "down";

}else if(sQuads[0][1] == sQuads[0][3] && +sQuads[0][3] < +sQuads[0][5]){
return "left";

}else{
return "up";

}

}




function addComplAnnot(pdfDoc,sQuads,strOnPgNum,strReplaceWth,intMaxCharForAnnot){
	var sQuadsTxtDir = getTextDirection(sQuads);

	for(yz=sQuads.length - 2;yz>=0;yz--){
		//app.alert("adding complex annot");
		//normalize sQuads
		
		//if(+sQuads[yz][0]>sQuads[yz][4]){sQuads[yz][0]=sQuads[yz][4];}
		//if(+sQuads[yz][1]<sQuads[yz][3]){sQuads[yz][1]=sQuads[yz][3];}
		//if(+sQuads[yz][2]<sQuads[yz][6]){sQuads[yz][2]=sQuads[yz][6];}
		//if(+sQuads[yz][5]>sQuads[yz][7]){sQuads[yz][5]=sQuads[yz][7];}
		//if(+sQuads[yz][0]<sQuads[yz][4]){sQuads[yz][4]=sQuads[yz][0];}
		//if(+sQuads[yz][1]>sQuads[yz][3]){sQuads[yz][3]=sQuads[yz][1];}
		//if(+sQuads[yz][2]>sQuads[yz][6]){sQuads[yz][6]=sQuads[yz][2];}
		//if(+sQuads[yz][5]<sQuads[yz][7]){sQuads[yz][7]=sQuads[yz][5];}
		
		//////
		var c1;
		var c5;
		var y11;
		var y15;

		if(sQuadsTxtDir == "right"){
			c1 = Math.round(sQuads[yz][1]*100)/100;
			c5 = Math.round(sQuads[yz][5]*100)/100;		

			y11 = Math.round(sQuads[yz+1][1]*100)/100;
			y15 = Math.round(sQuads[yz+1][5]*100)/100;
			//app.alert("right");
		}else if(sQuadsTxtDir == "down"){
			c1 = Math.round(sQuads[yz][0]*100)/100;
			c5 = Math.round(sQuads[yz][4]*100)/100;		

			y11 = Math.round(sQuads[yz+1][0]*100)/100;
			y15 = Math.round(sQuads[yz+1][4]*100)/100;
			//app.alert("down");
		}else if(sQuadsTxtDir == "left"){
			c1 = Math.round(sQuads[yz][5]*100)/100;
			c5 = Math.round(sQuads[yz][1]*100)/100;		

			y11 = Math.round(sQuads[yz+1][5]*100)/100;
			y15 = Math.round(sQuads[yz+1][1]*100)/100;
			//app.alert("left");
		}else if(sQuadsTxtDir == "up"){
			c1 = Math.round(sQuads[yz][4]*100)/100;
			c5 = Math.round(sQuads[yz][0]*100)/100;		

			y11 = Math.round(sQuads[yz+1][4]*100)/100;
			y15 = Math.round(sQuads[yz+1][0]*100)/100;
			//app.alert("up");
		}
		//////

				
		


		var nextVAve = Math.round(((+y11 + y15)/2)*100)/100;		

		//app.alert(sQuads[yz]);
		//app.alert(sQuads[yz+1]);
		
		//if vertical ave of next lies within current vertical bounds
		if(+nextVAve < +c1 && +nextVAve > +c5){
			//Right becomes next right. current max chars increase by next max chars
			//app.alert("current right becoming next right");

			/////
			if(sQuadsTxtDir == "right"){
				sQuads[yz][2] = sQuads[yz + 1][2];
				sQuads[yz][6] = sQuads[yz + 1][6];
				//app.alert("right");
			}else if(sQuadsTxtDir == "down"){
				sQuads[yz][3] = sQuads[yz + 1][3];//was 1
				sQuads[yz][7] = sQuads[yz + 1][7];//was 5
				//app.alert("down");
			}else if(sQuadsTxtDir == "left"){
				sQuads[yz][2] = sQuads[yz + 1][2];//was 0
				sQuads[yz][6] = sQuads[yz + 1][6];//was 4
				//app.alert("left");
			}else if(sQuadsTxtDir == "up"){
				sQuads[yz][3] = sQuads[yz + 1][3];
				sQuads[yz][7] = sQuads[yz + 1][7];
				//app.alert("up");
			}
			
			/////

			//carry over possible add quad above index 7
			if(sQuads[yz +1].length == 16){
				sQuads[yz].push(sQuads[yz+1][8]);
				sQuads[yz].push(sQuads[yz+1][9]);
				sQuads[yz].push(sQuads[yz+1][10]);
				sQuads[yz].push(sQuads[yz+1][11]);
				sQuads[yz].push(sQuads[yz+1][12]);
				sQuads[yz].push(sQuads[yz+1][13]);
				sQuads[yz].push(sQuads[yz+1][14]);
				sQuads[yz].push(sQuads[yz+1][15]);
	
			}

			
			//delete array from y + 1
			sQuads[yz + 1] = [1];
			intMaxCharForAnnot[yz] = intMaxCharForAnnot[yz] + intMaxCharForAnnot[yz+1];//+ 1 added 3-20
			intMaxCharForAnnot[yz + 1] = 0;
		}				

	}
//apply them separately, filling in max allowable words for that annot
	for(w=0;w<sQuads.length;w++){//for2
		
		if(sQuads[w].length>1){//if2
		//intMaxCharForAnnot[w]=intMaxCharForAnnot[w]-1;//subtracted 1 for text sizing




		
		//on valid quad which may include add quad
		if(sQuads[w].length < 16){//if68
		//get the string to apply to that quad. add - if not last valid quad and string is not " ".
		var intMaxWrds = intMaxCharForAnnot[w]-2;//-2 added for text sizing
		var strToApply = "";

		//place in the max number characters
		for(q=0;q<intMaxWrds;q++){//for6
			//if there are still characters to place into it
			if(strReplaceWth!=""){//if9
			
			//place first character into strToApply
			strToApply = strToApply.concat(strReplaceWth[0]);
			var strLastPlaced = strReplaceWth[0];	

			//remove first character
			var strReplaceWth2 = "";
			for(s=1;s<strReplaceWth.length;s++){
				strReplaceWth2 = strReplaceWth2.concat(strReplaceWth[s]);
			}					
			strReplaceWth = strReplaceWth2;
			
			//strReplaceWth = strReplaceWth.substring(1);	


			}//end if9
		}//end for6



		//fill in the last character with - if needed
		if(strReplaceWth !=""){
			if(strReplaceWth[0]!=" " && strLastPlaced != " " && notLastValidQuad(sQuads,w)==true){		
				if(strToApply !=""){//if34
					
					strReplaceWth = strToApply[strToApply.length - 1].concat(strReplaceWth);
					var strToApply2 = "";
					for(cw = 0;cw<strToApply.length-1;cw++){
						strToApply2 = strToApply2.concat(strToApply[cw]);
					}
					strToApply = strToApply2.concat("-");					
				}//end if34
			}
		}





		//place the quad with strtoapply
//first create new array from sQuads[w]
var sQuads2 = [];
sQuads2[0]=[sQuads[w][0],sQuads[w][1],sQuads[w][2],sQuads[w][3],sQuads[w][4],sQuads[w][5],sQuads[w][6],sQuads[w][7]]; 

//placed text should be in correct orientation with redaction

intInitPgRotate = pdfDoc.getPageRotation(strOnPgNum);//3-31
rotatePgToAnnot(pdfDoc,sQuads2,strOnPgNum);//3-31
		pdfDoc.addAnnot({
           		page:strOnPgNum,
          		type:"Redact",
           		quads:sQuads2,
           		overlayText:strToApply,
           		alignment:1,
           		repeat:false,
           		fillColor:color.white,
           		textColor:color.black,
           		textSize:0,
           		textFont:"TimesNewRoman"
      		});
		if(intInitPgRotate != pdfDoc.getPageRotation(strOnPgNum)){
pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,intInitPgRotate);///3-31
}


}else{//else68
//on valid quad which includes add quad
var strSub ="";
var sQuadsAdd = [];
sQuadsAdd[0] = [sQuads[w][8],sQuads[w][9],sQuads[w][10],sQuads[w][11],sQuads[w][12],sQuads[w][13],sQuads[w][14],sQuads[w][15]];
		//normalize the quad
		//if(+sQuadsAdd[0][0]>sQuadsAdd[0][4]){sQuadsAdd[0][0]=sQuadsAdd[0][4];}
		//if(+sQuadsAdd[0][1]<sQuadsAdd[0][3]){sQuadsAdd[0][1]=sQuadsAdd[0][3];}
		//if(+sQuadsAdd[0][2]<sQuadsAdd[0][6]){sQuadsAdd[0][2]=sQuadsAdd[0][6];}
		//if(+sQuadsAdd[0][5]>sQuadsAdd[0][7]){sQuadsAdd[0][5]=sQuadsAdd[0][7];}
		//if(+sQuadsAdd[0][0]<sQuadsAdd[0][4]){sQuadsAdd[0][4]=sQuadsAdd[0][0];}
		//if(+sQuadsAdd[0][1]>sQuadsAdd[0][3]){sQuadsAdd[0][3]=sQuadsAdd[0][1];}
		//if(+sQuadsAdd[0][2]>sQuadsAdd[0][6]){sQuadsAdd[0][6]=sQuadsAdd[0][2];}
		//if(+sQuadsAdd[0][5]<sQuadsAdd[0][7]){sQuadsAdd[0][7]=sQuadsAdd[0][5];}
		
		//////		

		var totalAnnotWidth;
		var addAnnotWidth;
		if(sQuadsTxtDir == "right"){
			totalAnnotWidth = (sQuadsAdd[0][2] - sQuadsAdd[0][0]) + (sQuads[w][2] - sQuads[w][0]);
			addAnnotWidth = sQuadsAdd[0][2] - sQuadsAdd[0][0];
			//app.alert("right");
		}else if(sQuadsTxtDir == "down"){
			totalAnnotWidth = (sQuadsAdd[0][1] - sQuadsAdd[0][3]) + (sQuads[w][1] - sQuads[w][3]);
			addAnnotWidth = sQuadsAdd[0][1] - sQuadsAdd[0][3];
			//app.alert("down");
		}else if(sQuadsTxtDir == "left"){
			totalAnnotWidth = (sQuadsAdd[0][0] - sQuadsAdd[0][2]) + (sQuads[w][0] - sQuads[w][2]);
			addAnnotWidth = sQuadsAdd[0][0] - sQuadsAdd[0][2];
			//app.alert("left");
		}else if(sQuadsTxtDir == "up"){
			totalAnnotWidth = (sQuadsAdd[0][3] - sQuadsAdd[0][1]) + (sQuads[w][3] - sQuads[w][1]);
			addAnnotWidth = sQuadsAdd[0][3] - sQuadsAdd[0][1];
			//app.alert("up");
		}
		//////



var intMaxLenAdd= Math.round((addAnnotWidth * intMaxCharForAnnot[w]) / totalAnnotWidth);
var intMaxLenOrig = intMaxCharForAnnot[w] - intMaxLenAdd;
intMaxLenAdd = intMaxLenAdd - 1;//-1 added for text sizing
intMaxLenOrig = intMaxLenOrig - 1;//-1 added for text sizing



var strReplaceWth2;
//create strSub for original quad, remove from strReplaceWth
for(q=0;q<intMaxLenOrig;q++){//for6
			//if there are still characters to place into it
			if(strReplaceWth!=""){//if9
			
			//place first character into strSub
			strSub = strSub.concat(strReplaceWth[0]);
			var strLastPlaced = strReplaceWth[0];	

			//remove first character
			strReplaceWth2="";
			for(s=1;s<strReplaceWth.length;s++){
				strReplaceWth2 = strReplaceWth2.concat(strReplaceWth[s]);
			}					
			strReplaceWth = strReplaceWth2;
			
			
			}//end if9
}//end for6
//fill in the last character with - if needed
		if(strReplaceWth !=""){
			if(strReplaceWth[0]!=" " && strLastPlaced != " "){		
				
					
					strReplaceWth = strSub[strSub.length - 1].concat(strReplaceWth);
					var strSub2 = "";
					for(cw = 0;cw<strSub.length-1;cw++){
						strSub2 = strSub2.concat(strSub[cw]);
					}
					strSub = strSub2.concat("-");					
				
			}
		}


	
var sQuads4 = [];
sQuads4[0]=[sQuads[w][0],sQuads[w][1],sQuads[w][2],sQuads[w][3],sQuads[w][4],sQuads[w][5],sQuads[w][6],sQuads[w][7]];
//placed text should be in correct orientation with redaction
intInitPgRotate = pdfDoc.getPageRotation(strOnPgNum);//3-31
rotatePgToAnnot(pdfDoc,sQuads4,strOnPgNum);//3-31

pdfDoc.addAnnot({
           page:strOnPgNum,
           type:"Redact",
           quads:sQuads4,
           overlayText:strSub,
           alignment:1,
           repeat:false,
           fillColor:color.white,
           textColor:color.black,
           textSize:0,
           textFont:"TimesNewRoman"
      });
if(intInitPgRotate != pdfDoc.getPageRotation(strOnPgNum)){
pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,intInitPgRotate);///3-31
}

//original annotation placed. the sub quad remains


//if original is last valid quad then place in add quad. otherwise join add quad to next valid quad i.e. .length>1
//first create strSub for the add quad
if(notLastValidQuad(sQuads,w)==false){//if62
strSub = "";
for(q=0;q<intMaxLenAdd;q++){//for6
			//if there are still characters to place into it
			if(strReplaceWth!=""){//if9
			
			//place first character into strSub
			strSub = strSub.concat(strReplaceWth[0]);
			var strLastPlaced = strReplaceWth[0];	

			//remove first character
			strReplaceWth2="";
			for(s=1;s<strReplaceWth.length;s++){
				strReplaceWth2 = strReplaceWth2.concat(strReplaceWth[s]);
			}					
			strReplaceWth = strReplaceWth2;
			
			
			}//end if9
}//end for6
	
//placed text should be in correct orientation with redaction

intInitPgRotate = pdfDoc.getPageRotation(strOnPgNum);//3-31
rotatePgToAnnot(pdfDoc,sQuadsAdd,strOnPgNum);//3-31
pdfDoc.addAnnot({
           page:strOnPgNum,
           type:"Redact",
           quads:sQuadsAdd,
           overlayText:strSub,
           alignment:1,
           repeat:false,
           fillColor:color.white,
           textColor:color.black,
           textSize:0,
           textFont:"TimesNewRoman"
});
if(intInitPgRotate != pdfDoc.getPageRotation(strOnPgNum)){
pdfDoc.setPageRotations(strOnPgNum,strOnPgNum,intInitPgRotate);///3-31
}

}else{//else62
//there's another valid quad. update sQuads, and maxchararray for next valid index
for(hc=sQuads.length-1;hc>w;hc--){
if(sQuads[hc].length >1){
var nextValidQuadInd = hc;
}
}

intMaxCharForAnnot[nextValidQuadInd] = intMaxCharForAnnot[nextValidQuadInd] + intMaxLenAdd;
//squadsadd0 has been normalized. normalize sQuads[nextvalidquadind]
		//if(+sQuads[nextValidQuadInd][0]>sQuads[nextValidQuadInd][4]){sQuads[nextValidQuadInd][0]=sQuads[nextValidQuadInd][4];}
		//if(+sQuads[nextValidQuadInd][1]<sQuads[nextValidQuadInd][3]){sQuads[nextValidQuadInd][1]=sQuads[nextValidQuadInd][3];}
		//if(+sQuads[nextValidQuadInd][2]<sQuads[nextValidQuadInd][6]){sQuads[nextValidQuadInd][2]=sQuads[nextValidQuadInd][6];}
		//if(+sQuads[nextValidQuadInd][5]>sQuads[nextValidQuadInd][7]){sQuads[nextValidQuadInd][5]=sQuads[nextValidQuadInd][7];}
		//if(+sQuads[nextValidQuadInd][0]<sQuads[nextValidQuadInd][4]){sQuads[nextValidQuadInd][4]=sQuads[nextValidQuadInd][0];}
		//if(+sQuads[nextValidQuadInd][1]>sQuads[nextValidQuadInd][3]){sQuads[nextValidQuadInd][3]=sQuads[nextValidQuadInd][1];}
		//if(+sQuads[nextValidQuadInd][2]>sQuads[nextValidQuadInd][6]){sQuads[nextValidQuadInd][6]=sQuads[nextValidQuadInd][2];}
		//if(+sQuads[nextValidQuadInd][5]<sQuads[nextValidQuadInd][7]){sQuads[nextValidQuadInd][7]=sQuads[nextValidQuadInd][5];}

//left off 4-2

			/////
			if(sQuadsTxtDir == "right"){
				sQuads[nextValidQuadInd][0] = sQuadsAdd[0][0];
				sQuads[nextValidQuadInd][4] = sQuadsAdd[0][4];
				//app.alert("right");
			}else if(sQuadsTxtDir == "down"){
				sQuads[nextValidQuadInd][1] = sQuadsAdd[0][1];//was 3
				sQuads[nextValidQuadInd][5] = sQuadsAdd[0][5];//was 7
				//app.alert("down");
			}else if(sQuadsTxtDir == "left"){
				sQuads[nextValidQuadInd][0] = sQuadsAdd[0][0];//was 2
				sQuads[nextValidQuadInd][4] = sQuadsAdd[0][4];//was 6
				//app.alert("left");
			}else if(sQuadsTxtDir == "up"){
				sQuads[nextValidQuadInd][1] = sQuadsAdd[0][1];
				sQuads[nextValidQuadInd][5] = sQuadsAdd[0][5];
				//app.alert("up");
			}
			
			/////



}//end if62




}//end if68

		}//end if2

	}//end for2
return;

}


function notLastValidQuad(sQuads,w){
var intCount = sQuads.length;
if(w==sQuads.length - 1){
return false;

}else{
for(kw = w + 1;kw < sQuads.length;kw++){
if(sQuads[kw].length >1){return true;}
}
return false;
}


}



function getSupersQuads(sQuads){
//left,top,right,top,left,bottom,right,bottom
//-   ,+  ,+    ,+  ,-   ,-    , +    ,-
var l1 = sQuads[0][0];
var t2 = sQuads[0][1];
var r3 = sQuads[0][2];
var t4 = sQuads[0][3];
var l5 = sQuads[0][4];
var b6 = sQuads[0][5];
var r7 = sQuads[0][6];
var b8 = sQuads[0][7];

for(h1=1;+h1<+sQuads.length;h1++){

if(+l1 > +sQuads[h1][0]){

l1 = sQuads[h1][0];
}
if(+t2 < +sQuads[h1][1]){
t2 = sQuads[h1][1];
}
if(+r3 < +sQuads[h1][2]){
r3 = sQuads[h1][2];
}
if(+t4 < +sQuads[h1][3]){
t4 = sQuads[h1][3];
}
if(+l5 > +sQuads[h1][4]){
l5 = sQuads[h1][4];
}
if(+b6 > +sQuads[h1][5]){
b6 = sQuads[h1][5];
}
if(+r7 < +sQuads[h1][6]){
r7 = sQuads[h1][6];
}
if(+b8 > +sQuads[h1][7]){
b8 = sQuads[h1][7];
}

}

var supsQuads = [];
supsQuads[0] = [l1,t2,r3,t4,l5,b6,r7,b8];

return supsQuads;
}










var markedReport1 = app.trustedFunction(function()
{
app.beginPriv();

var th = app.thermometer;
var strReport = "Report: ";

//open the pdf.
var mPdfLocTBVal = this.getField("mPdfLocationTB").value; 
var mPdfDoc = app.openDoc({cPath:mPdfLocTBVal,bHidden:true});

//for each redaction annotation in the pdf, generate row in report array.
var reportArray = [];
var rowArray = [];
rowArray = ["Text to be Replaced","Replace with this Text","Page Number","Page Instance"];
reportArray.push(rowArray);
mPdfDoc.syncAnnotScan();
var redactAnnotations = mPdfDoc.getAnnots();

th.duration = redactAnnotations.length;
th.begin();

for (var i =0;+i<+redactAnnotations.length;i++){//for 1
th.value= +i + 1;
th.text = "Processing item " + (+i +1) + " of " + redactAnnotations.length;

if (redactAnnotations[i].type == "Redact"){//if 1
//process the annotation

rowArray = [];
var txtToReplace = "";
var pgInst = "";
var pgNum= redactAnnotations[i].page;//zero based

//get txtToReplace
txtToReplace = getTxtToReplace(mPdfDoc,redactAnnotations[i],pgNum);


//get pgInst
pgInst = getPgInst(mPdfDoc,redactAnnotations[i],txtToReplace,pgNum);


rowArray = [txtToReplace,"",+pgNum +1,pgInst];
reportArray.push(rowArray);
}//end if 1
}//end for 1


//create tab delimited txt file from reportArray
//get the name and save path of the txt file
//var strMReportPath;
//strMReportPath = this.getField("mPdfLocationTB").value.substring(0,this.getField("mPdfLocationTB").value.length - 4) + "_MReport.txt";

this.createDataObject("MReport.txt","Text to be Replaced\tReplace with this Text\tPage Number\tPage Instance");

// Get the file stream object of the embedded file
		var oFile = this.getDataObjectContents("MReport.txt");

		// Convert to a string
		var sfsmReport = util.stringFromStream(oFile, "utf-8");
		// for each row in array append row to the end, using tabs to separate info
		for(b=1;b< reportArray.length;b++){
			//prepare reportArray row contents for export
			var b0 = reportArray[b][0].replace(/\r\n|\r|\n|\t/g,"");
			var b1 = reportArray[b][1];
			var b2 = reportArray[b][2];
			var b3 = reportArray[b][3];
			var sfsmReport = sfsmReport + "\r\n" + b0 + "\t" + b1 + "\t" + b2 + "\t" + b3;
		}

		// Convert back to a file stream
		var oFile2 = util.streamFromString(sfsmReport, "utf-8");

		// Now "overwrite" budget.xls
		this.setDataObjectContents("MReport.txt", oFile2);


this.exportDataObject("MReport.txt");

this.removeDataObject("MReport.txt");

mPdfDoc.closeDoc();

if(strReport == "Report: "){
	strReport = "Text file generated and saved.";
}
this.getField("mReportTB").value = strReport;
th.end();
app.alert("Report successfully generated.",3,0,"Success");
app.endPriv();
});


function getTxtToReplace(mPdfDoc,rdctAnnot,pg){//get underlying text.
//get number of words on the page
var len = mPdfDoc.getPageNumWords(pg);
var txt = "";
//enumerate the words until in quad area, get words in quad area
for(z=0;+z<+len;z++){//for1
var wrdQuads = mPdfDoc.getPageNthWordQuads(pg,z);


var wrdQuadsWrd = mPdfDoc.getPageNthWord(pg,z,true);

if(isWithinQuads(wrdQuads,rdctAnnot.quads,wrdQuadsWrd)==true){//not working correctly
//app.alert("is within quads");

txt = txt.concat(mPdfDoc.getPageNthWord(pg,z,false));
//app.alert(txt);

}else if(txt !=""){
return txt;
}

}//end for1

return txt;
}


function getPgInst(mPdfDoc,rdctAnnot,txtReplace,pg){
//get number of words on the page
var len = mPdfDoc.getPageNumWords(pg);
var strToFind = txtReplace.replace(/[^0-9a-zA-Z]/g,"");
var intInstanceCount = 0;
for(o=0;+o<+len;o++){//for 4. for each word in the page

var wd = mPdfDoc.getPageNthWord(pg,o,true).replace(/[^0-9a-zA-Z]/g,"");

var a = 0;//expand wd if part of strToFind, until equal in length.

while(strToFind.toUpperCase().indexOf(wd.toUpperCase()) !=-1 && +wd.length<+strToFind.length && o+(a+1) < +len){
//ie while not equal in length, and more words are available on page
a = a+1;
wd= wd.concat(mPdfDoc.getPageNthWord(pg,o+ a,true).replace(/[^0-9a-zA-Z]/g,""));
}//end while


if(wd.toUpperCase()===strToFind.toUpperCase()){//if1
	intInstanceCount = +intInstanceCount + 1;
	if(isWithinQuads(mPdfDoc.getPageNthWordQuads(pg,o),rdctAnnot.quads)){
		return intInstanceCount;
	}
}//end if1
}//end for 4

}



function isWithinQuads(subQuad,quad,wrdQuadsWrd){

//l,t,r,t,l,b,r,b
//+,-,-,-,+,+,-,+
for(cs=0;+cs<+subQuad.length;cs++){//for5
//normalize the quad
if(+subQuad[cs][0]>subQuad[cs][4]){subQuad[cs][0]=subQuad[cs][4];}
if(+subQuad[cs][1]<subQuad[cs][3]){subQuad[cs][1]=subQuad[cs][3];}
if(+subQuad[cs][2]<subQuad[cs][6]){subQuad[cs][2]=subQuad[cs][6];}
if(+subQuad[cs][5]>subQuad[cs][7]){subQuad[cs][5]=subQuad[cs][7];}
if(+subQuad[cs][0]<subQuad[cs][4]){subQuad[cs][4]=subQuad[cs][0];}
if(+subQuad[cs][1]>subQuad[cs][3]){subQuad[cs][3]=subQuad[cs][1];}
if(+subQuad[cs][2]>subQuad[cs][6]){subQuad[cs][6]=subQuad[cs][2];}
if(+subQuad[cs][5]<subQuad[cs][7]){subQuad[cs][7]=subQuad[cs][5];}


var subL1 = subQuad[cs][0];//+= equal to or greater than left and less than right
var subT2= subQuad[cs][1];//-= equal or less than top
var subR3= subQuad[cs][2];//-=
var subB6= subQuad[cs][5];//+= equal or greater than bottom

var aveL1R3 = Math.round(((+subL1 + subR3) /2)*100)/100; //greater than left, less than right
var aveT2B6 = Math.round(((+subT2 + subB6) /2)*100)/100; //lesser than top, greater than bottom

//if(wrdQuadsWrd=="Site"){app.alert(aveL1R3 + "  " + aveT2B6);}

for(l=0;+l<quad.length;l++){//for2
//normalize the quad
if(+quad[l][0]>quad[l][4]){quad[l][0]=quad[l][4];}
if(+quad[l][1]<quad[l][3]){quad[l][1]=quad[l][3];}
if(+quad[l][2]<quad[l][6]){quad[l][2]=quad[l][6];}
if(+quad[l][5]>quad[l][7]){quad[l][5]=quad[l][7];}
if(+quad[l][0]<quad[l][4]){quad[l][4]=quad[l][0];}
if(+quad[l][1]>quad[l][3]){quad[l][3]=quad[l][1];}
if(+quad[l][2]>quad[l][6]){quad[l][6]=quad[l][2];}
if(+quad[l][5]<quad[l][7]){quad[l][7]=quad[l][5];}


var L1 = quad[l][0];
var T2= quad[l][1];
var R3= quad[l][2];
var B6= quad[l][5];

if(Math.round(aveL1R3*100)/100 > Math.round(L1*100)/100 && Math.round(aveL1R3*100)/100 < Math.round(R3*100)/100 && Math.round(aveT2B6*100)/100 < Math.round(T2*100)/100 && Math.round(aveT2B6*100)/100 > Math.round(B6*100)/100){
	
	return true;
}
}//end for2


}//end for5



return false;

}


