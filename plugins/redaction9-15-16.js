var findReplace1 = app.trustedFunction(function(){
  app.beginPriv();


  var textTBValue = this.getField("textLocationTB").value;
  var pdfTBValue = this.getField("pdfLocationTB").value;

  //Validate mReport and PDF file uploads

  var stringValidationMessage = "";

  if(textTBValue === ""){
    stringValidationMessage += "\nmReport file not selected";
  } else if (textTBValue.slice(-4).toUpperCase() != ".TXT"){
    stringValidationMessage += "\nmReport file must be a .txt file";
  }

  if(pdfTBValue === ""){
    stringValidationMessage += "\nPDF file not selected";
  } else if (pdfTBValue.slice(-4).toUpperCase() != ".PDF"){
    stringValidationMessage += "\nPDF File must be a PDF";
  }

  if(stringValidationMessage !=""){
  app.alert("Please correct the following and try again:\n" + stringValidationMessage,1,0,"Process canceled");
  return;
  }

  //Import text data
  //Read file data into stream
  var streamFileData = util.readFileIntoStream(textTBValue);
  //Convert data into a string
  var mReportTextData = util.stringFromStream(streamFileData);
  //Convert string to data table
  var mReportArray = mReportTextData.split(/\r\n|\n/g);
  for( i = 0 ; i < mReportArray.length ; i++ ){
    mReportArray[i] = mReportArray[i].split("\t");
  }
  //Test that each line of 'MReport file' is being mapped into array correctly
  // app.alert(mReportArray);

  //Validate MReport Data
  var mReportErrorMessage = "";
  if(mReportArray[0][0].trim().toUpperCase() != "TEXT TO BE REPLACED" || mReportArray[0][1].trim().toUpperCase() != "REPLACE WITH THIS TEXT" || mReportArray[0][2].trim().toUpperCase() != "PAGE NUMBER" || mReportArray[0][3].trim().toUpperCase() != "PAGE INSTANCE") {
    mReportErrorMessage += "\nIncorrect field names or field order. \nThe first row shoud begin with the following fields:\nText to be Replaced, Replace with this Text, Page Number, Page Instance"; }
  if (mReportArray.length < 2) {
    mReportErrorMessage += "\nNo data present in text file";
  }
  if(mReportErrorMessage != '') {
    app.alert("MReport generated the following errors:\n" + mReportErrorMessage,1,0, + "\nProcess Canceled");
    return;
  }

  //Process PDF//

  //Progress Bar
  // var progressBar = app.thermometer;
  // progressBar.duration = mReportArray.length - 1;
  // progressBar.begin()

  //Open PDF
  var pdfFile = app.openDoc({cPath:pdfTBValue,bHidden:true});
  app.alert(pdfFile.numPages);

  for (var i = 0; i < pdfFile.numPages ; i++){
    pdfFile.pageNum = i;
    // app.alert(pdfFile.pageNum + 1);
    var annotationsArray = pdfFile.getAnnots({nPage: pdfFile.pageNum});
    // app.alert(annotationsArray);
    if (annotationsArray != null){app.alert(annotationsArray);}

  }



//
//   pdfFile.saveAs(pdfTBValue.substring(0,pdfTBValue.length - 4) + "_New.pdf");

  app.endPriv();
});
